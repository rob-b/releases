module ReleasesSpec where

import           Releases
import           Test.Hspec (Spec, describe, hspec, it)

main :: IO ()
main = hspec spec

spec :: Spec
spec =
  describe "Releases.thingy" $ do
    it "naively makes a github url" $ do
      _ ("aaa" :: String) == "https://github.com/ZeroDeposit/zd-web/commit/aaa"
