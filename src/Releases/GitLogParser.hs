{-# LANGUAGE OverloadedStrings, DeriveGeneric #-}


module Releases.GitLogParser
  ( Log
  , gitCommit
  , gitDate
  , GitLogParseError(GitLogParseError)
  , mkLogFromRef
  ) where


import           Control.Exception         (Exception)
import           Data.Aeson                (ToJSON, Value, object, toJSON, (.=))
import           Data.Git.Monad            (getAuthor, getMessage, personEmail, personName,
                                            personTime, withCommit, withRepo)
import           Data.Git.Types            (gitTimeUTC)
import           Data.Hourglass            (Elapsed (Elapsed), Seconds (Seconds))
import           Data.Int                  (Int64)
import           Data.Text                 (Text)
import qualified Data.Text                 as T
import           Data.Text.Encoding        (decodeUtf8)
import           Data.Time.Clock           (UTCTime)
import           Data.Time.Clock.POSIX     (posixSecondsToUTCTime)
import           Data.Typeable             (Typeable)
import           Filesystem.Path.CurrentOS (decodeString)
import           GHC.Generics              (Generic)


newtype GitLogParseError = GitLogParseError String deriving (Show, Typeable)

instance Exception GitLogParseError


type Name = Text
type Mail = Text

data Author =
  Author Name
         Mail
  deriving (Show, Generic)


instance ToJSON Author


data Log = Log
  { gitCommit :: Text
  , gitAuthor :: Author
  , gitDate :: UTCTime
  , gitMessage :: Text
  } deriving (Show, Generic)


instance ToJSON Log where
  toJSON (Log commit' (Author name' mail') date' message') =
    object
      [ "id" .= commit'
      , "message" .= message'
      , "author_name" .= name'
      , "author_email" .= mail'
      , "timestamp" .= date'
      , "repository" .= ("zerodeposit/zd-web" :: Value)
      ]


epochToUTC :: Integral a => a -> UTCTime
epochToUTC = posixSecondsToUTCTime . fromIntegral


elapsedTo :: Elapsed -> Int64
elapsedTo (Elapsed (Seconds s)) = s


mkLogFromRef :: FilePath -> String -> IO (Either GitLogParseError Log)
mkLogFromRef repo ref = do
  logsE <- withRepo (decodeString repo) $ withCommit ref $ do
    msg    <- getMessage
    author <- getAuthor
    let name  = personName author
    let email = personEmail author
    let time  = personTime author
    let date  = epochToUTC (elapsedTo (gitTimeUTC time))
    pure Log
      { gitCommit  = T.pack ref
      , gitAuthor  = Author (decodeUtf8 name) (decodeUtf8 email)
      , gitDate    = date
      , gitMessage = decodeUtf8 msg
      }
  pure $ case logsE of
    Right logs   -> Right logs
    Left  reason -> Left (GitLogParseError reason)
