{-# LANGUAGE OverloadedStrings #-}
module Releases.Heroku
  ( HerokuToken(HerokuToken)
  , commitHashesForAllReleases
  ) where

import           Control.Exception    (SomeException)
import           Control.Lens         ((^..), (^?))
import           Control.Monad.Catch  (MonadThrow)
import           Data.Aeson           (Value)
import           Data.Aeson.Lens      (AsValue, key, values, _String)
import           Data.ByteString      (ByteString)
import           Data.Maybe           (fromMaybe)
import           Data.Monoid          ((<>))
import           Data.Text            (Text)
import           Data.Text.Encoding   (encodeUtf8)
import           Network.HTTP.Conduit (parseUrlThrow)
import           Network.HTTP.Simple  (Request, getResponseBody, httpJSON, setRequestHeaders,
                                       setRequestMethod, setRequestPath)
import           UnexceptionalIO      (UIO)
import qualified UnexceptionalIO      as UIO


newtype HerokuToken = HerokuToken
  { unHerokuToken :: ByteString
  } deriving (Show)


herokuApiVersion :: ByteString
herokuApiVersion = "application/vnd.heroku+json; version=3"


herokuEndpoint :: (MonadThrow m) => m Request
herokuEndpoint = parseUrlThrow "https://api.heroku.com"


herokuSlugEndpoint :: MonadThrow f => ByteString -> f Request
herokuSlugEndpoint slugId =
  setRequestPath ("/apps/zerodeposit-uat/slugs/" <> slugId) <$> herokuEndpoint


herokuReleasesEndpoint :: MonadThrow f => f Request
herokuReleasesEndpoint =
  setRequestPath "/apps/zerodeposit-uat/releases" <$> herokuEndpoint


getResponse :: HerokuToken -> UIO Request -> UIO (Either SomeException Value)
getResponse token request = do
  request'' <- request
  let request' =
        setRequestMethod "GET" $
        setRequestHeaders
          [ ("Authorization", "Bearer " <> unHerokuToken token)
          , ("Accept", herokuApiVersion)
          , ("Range", "version ..;order=desc,max=3")
          ]
          request''
  response <- UIO.fromIO $ httpJSON request'
  pure $ fmap getResponseBody response


extractReleaseSlugs :: (AsValue s) => s -> [Text]
extractReleaseSlugs releases = releases ^.. values . key "slug" . key "id" . _String


commitHashesForAllReleases
  :: (UIO.Unexceptional m, Monad m)
  => HerokuToken -> m (Either SomeException [Text])
commitHashesForAllReleases herokuToken' = do
  rs <- UIO.liftUIO $ getReleases herokuToken'
  case rs of
    Left exc -> pure $ Left exc
    Right releases -> do
      let slugIds = extractReleaseSlugs releases
      slugs <- sequence <$> traverse (UIO.liftUIO . getSlug herokuToken') slugIds
      pure $ (fmap . fmap) extractSlugCommitHash slugs


getReleases :: HerokuToken -> UIO (Either SomeException Value)
getReleases token = getResponse token (UIO.unsafeFromIO herokuReleasesEndpoint)


getSlug :: HerokuToken -> Text -> UIO (Either SomeException Value)
getSlug token slugId = getResponse token (UIO.unsafeFromIO (herokuSlugEndpoint $ encodeUtf8 slugId))


extractSlugCommitHash :: (AsValue s) => s -> Text
extractSlugCommitHash s = fromMaybe "" (s ^? key "commit" . _String)
