{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
module Releases
     where


import           Control.Applicative        ((<|>))
import           Control.Error              (errLn, note)
import           Control.Exception          (SomeException, fromException, toException)
import           Control.Monad              (forM, forM_)
import           Control.Monad.Except       (runExceptT, throwError)
import           Control.Monad.Trans        (lift)
import           Data.Aeson                 (ToJSON, Value (String), encode, object, toJSON, (.=))
import           Data.Bifunctor             (first)
import           Data.ByteString            (ByteString)
import qualified Data.ByteString            as B
import qualified Data.ByteString.Lazy.Char8 as C
import           Data.Maybe                 (fromJust, listToMaybe)
import           Data.Monoid                ((<>))
import           Data.String                (IsString)
import           Data.Text                  (Text)
import qualified Data.Text                  as T
import           Data.Text.Encoding         (decodeUtf8, encodeUtf8)
import           Data.Time.Clock            (UTCTime)
import           Data.Time.Format           (ParseTime, defaultTimeLocale, parseTimeM)
import           GHC.Generics               (Generic)
import           Network.HTTP.Conduit       (HttpException (HttpExceptionRequest))
import           Network.HTTP.Simple        (Request, getResponseBody, httpJSON, setRequestBodyLBS,
                                             setRequestHeaders, setRequestMethod, setRequestPath)
import           Releases.GitLogParser      (GitLogParseError (GitLogParseError), Log, gitCommit,
                                             gitDate, mkLogFromRef)
import           Releases.Heroku            (HerokuToken (HerokuToken), commitHashesForAllReleases)
import           System.Environment         (lookupEnv)
import           System.Exit                (ExitCode (ExitFailure, ExitSuccess), exitFailure)
import           System.Process.Typed       (proc, readProcess)
import           UnexceptionalIO            (UIO)
import qualified UnexceptionalIO            as UIO

newtype URL = URL
  { unURL :: ByteString
  } deriving (Show, Generic)


data ReleasePayload = ReleasePayload
  { releaseRef     :: Maybe Text
  , releaseURL     :: URL
  , releaseDate    :: UTCTime
  , releaseRefs    :: Maybe [Text]
  , releaseCommits :: [Log]
  } deriving (Generic, Show)


instance ToJSON ReleasePayload where
  toJSON (ReleasePayload ref' url' date' refs' commits') =
    case refs' of
      Nothing -> object
          [ "ref" .= ref'
          , "dateReleased" .= date'
          , "url" .= url'
          , "commits" .= commits'
          ]
      Just _ ->object
          [ "ref" .= ref'
          , "dateReleased" .= date'
          , "url" .= url'
          , "refs" .= refs'
          , "commits" .= commits'
          ]


instance ToJSON URL where
  toJSON u = String . decodeUtf8 $ unURL u


lookupEnv' :: String -> IO (Maybe B.ByteString)
lookupEnv' k = do
  token' <- lookupEnv k
  pure $ C.toStrict . C.pack <$> token'


putResponse :: SentryToken -> Request -> ReleasePayload -> IO Value
putResponse token request payload = do
  let request' =
        setRequestMethod "PUT" $
        setRequestBodyLBS (encode payload) $
        setRequestHeaders
          [("Content-Type", "application/json"), ("Authorization", "Bearer " <> unSentryToken token)]
          request
  response <- httpJSON request'
  pure $ getResponseBody response :: IO Value


sentryGet :: SentryToken -> Request -> IO Value
sentryGet token request = do
  let request' =
        setRequestMethod "GET" $
        setRequestHeaders
          [("Content-Type", "application/json"), ("Authorization", "Bearer " <> unSentryToken token)]
          request
  response <- httpJSON request'
  pure $ getResponseBody response :: IO Value


sentryEndpoint :: Request
sentryEndpoint = "https://sentry.io"


viewRelease :: SentryToken -> ByteString -> IO Value
viewRelease token hash' = sentryGet token sentryReleaseEndpoint
  where
    sentryReleaseEndpoint =
      setRequestPath ("/api/0/organizations/zero-deposit/releases/" <> hash' <> "/") sentryEndpoint


updateRelease :: SentryToken -> ByteString -> ReleasePayload -> IO Value
updateRelease token hash' = putResponse token sentryReleaseEndpoint
  where
    sentryReleaseEndpoint =
      setRequestPath ("/api/0/organizations/zero-deposit/releases/" <> hash' <> "/") sentryEndpoint


windows :: Int -> [a] -> [[a]]
windows _ [] = []
windows n xz@(_:xs)
  | length v < n = []
  | otherwise = v : windows n xs
  where
    v = take n xz


---------------------------------------------------------------------------------------------------
-- | convert list of commits into git range syntax:
--
-- >> makeCommitRange ['aaa', 'bbb', 'ccc']
-- ['aaa...bbb', 'bbb...ccc']
makeCommitRange :: [Text] -> [Text]
makeCommitRange cs = makeRange <$> windows 2 cs
  where
    makeRange :: [Text] -> Text
    makeRange xs = T.intercalate "..." (reverse xs)


unique
  :: Eq a
  => [a] -> [a]
unique []     = []
unique (x:xs) = x : unique (filter (x /=) xs)


getReleaseParams :: Options -> UIO (Either SomeException [ReleasePayload])
getReleaseParams options =
  UIO.unsafeFromIO $
  -- query heroku for app releases, extract git refs from those releases
  commitHashesForAllReleases (herokuToken options) >>= \case
    Left xs -> pure $ Left xs
    Right commits -> do
      let ranges = makeCommitRange (unique commits)
      releaseParams <- forM ranges (mkReleaseFromRef (repoPath options))
      pure $ sequence releaseParams


mkReleaseFromRef :: FilePath -> Text -> IO (Either SomeException ReleasePayload)
mkReleaseFromRef repo ref =
  runExceptT $ do
    let logsForE = getLogsForRelease repo (T.unpack ref)
    -- The Left of `getLogsForRelease` is of the wrong kind so we need to convert from
    -- GitLogParseError to SomeException. We lift the whole result to ExceptT at the same
    -- time
    logsFor <- lift (first toException <$> logsForE)
    logs <- either throwError pure logsFor
    let logMaybe = listToMaybe logs
    let logEither = note (GitLogParseError $ "No refs for given range: " ++ T.unpack ref) logMaybe
    topLog <- either throwError pure (first toException logEither)
    return
      ReleasePayload
      { releaseRef = Just (gitCommit topLog)
      , releaseURL = URL $ encodeUtf8 (urlForCommit (gitCommit topLog))
      , releaseDate = gitDate topLog
      , releaseRefs = Nothing
      , releaseCommits = logs
      }


getLogsForRelease :: FilePath -> String -> IO (Either GitLogParseError [Log])
getLogsForRelease repo ref = do
  let processConfig =
        proc
          "git"
          [ "-C"
          , repo
          , "log"
          , "--pretty=format:%h"
          , ref
          ]
  (exitCode, stdout, stderr) <- readProcess processConfig
  case exitCode of
    ExitFailure _ -> pure $ Left (GitLogParseError (C.unpack stderr ++ " " ++ ref))
    ExitSuccess -> do
      logs <- sequence <$> traverse (mkLogFromRef repo . C.unpack) (C.lines stdout)
      pure logs


urlForCommit :: (IsString m, Monoid m) => m -> m
urlForCommit url = "https://github.com/ZeroDeposit/zd-web/commit/" <> url


parseISO8601 :: (Monad m, ParseTime t)
             => String
             -> m t
parseISO8601 = parseTimeM True defaultTimeLocale "%Y-%m-%dT%H:%M:%S%z"


newtype SentryToken = SentryToken
  { unSentryToken :: ByteString
  } deriving (Show)


data Options = Options
  { sentryToken :: SentryToken
  , herokuToken :: HerokuToken
  , repoPath    :: FilePath
  } deriving (Show)


mkOptions :: IO Options
mkOptions = do
  herokuAuthToken <-
    lookupEnv' "HEROKU_AUTH_TOKEN" >>= \case
      Nothing -> putStrLn "You must set HEROKU_AUTH_TOKEN env var" >> exitFailure
      Just t -> pure (HerokuToken t)
  sentryAuthToken <-
    lookupEnv' "SENTRY_AUTH_TOKEN" >>= \case
      Nothing -> putStrLn "You must set SENTRY_AUTH_TOKEN env var" >> exitFailure
      Just t -> pure (SentryToken t)
  pure
    Options
    { sentryToken = sentryAuthToken
    , herokuToken = herokuAuthToken
    , repoPath = "/Users/rob/Projects/zerodeposit/zd-web/.git"
    }


getReleaseRefFromParams :: ReleasePayload -> ByteString
getReleaseRefFromParams params = encodeUtf8 . fromJust $ releaseRef params


httpFailure :: SomeException -> Maybe String
httpFailure e = case fromException e of
  Just (HttpExceptionRequest _ content) -> Just $ show content
  _                                     -> Nothing


gitFailure :: SomeException -> Maybe String
gitFailure e = case fromException e of
  Just (GitLogParseError content) -> Just $ "Problem with git history: " <> content
  _                               -> Nothing


reportError :: Maybe String -> IO ()
reportError ms = errLn $ maybe "An unexpected error occurred." T.pack ms


libMain :: IO ()
libMain = do
  options <- mkOptions
  UIO.liftUIO (getReleaseParams options) >>= \case
    Left exc -> reportError (httpFailure exc <|> gitFailure exc)
    Right payloads ->
      forM_ payloads $ \payload -> do
          C.putStrLn $ encode payload
          putStrLn "______"
          -- response <- updateRelease (sentryToken options) (getReleaseRefFromParams payload) payload
          -- C.putStrLn $ encode response
          -- putStrLn "******"
          -- release <- viewRelease (sentryToken options) (getReleaseRefFromParams payload)
          -- C.putStrLn $ encode release
          -- putStrLn "******"
